<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\FavoriteRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="main_index")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @param FavoriteRepository $favoriteRepository
     * @return Response
     */
    public function index(Request $request, ApiContext $apiContext, FavoriteRepository $favoriteRepository): Response
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $records = [];

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $filter = $form->getData();
                $records = $apiContext->getAllByFilter($filter['query'], $filter['sort'], $filter['limit'])['data']['children'];
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }
        }

        foreach ($records as $key => $record){
            $records[$key]['favorites'] = $favoriteRepository->countByRedditId($record['data']['name']);
        }

        return $this->render('main/index.html.twig', [
            'form' => $form->createView(),
            'records' => $records
        ]);
    }
}

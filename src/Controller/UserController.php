<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Form\RegisterType;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller
{
    /**
     * @Route("/register", name="user_register")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserHandler $userHandler
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @return Response
     */
    public function register(
        Request $request,
        EntityManagerInterface $manager,
        UserHandler $userHandler,
        UserPasswordEncoderInterface $userPasswordEncoder
    ): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('main_index');
        }

        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($userPasswordEncoder->encodePassword($user, $user->getPlainPassword()));

            $manager->persist($user);
            $manager->flush();

            $userHandler->makeUserSession($user);

            return $this->redirectToRoute('main_index');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="user_login")
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @return Response
     */
    public function login(
        Request $request,
        UserRepository $userRepository,
        UserHandler $userHandler,
        UserPasswordEncoderInterface $userPasswordEncoder
    ): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('main_index');
        }

        $error = false;

        $form = $this->createForm(LoginType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $user = $userRepository->findOneByUsername($data['email']);

            if ($user) {

                if ($userPasswordEncoder->isPasswordValid($user, $data['plainPassword'])) {
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('main_index');
                }

                $error = 'Введены неправильные данные';
            } else {
                $error = 'Такой пользователь не зарегестрирован';
            }
        }

        return $this->render('user/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\User;
use App\Form\AddFavoriteType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\FavoriteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/favorite")
 *
 * Class FavoriteController
 * @package App\Controller
 */
class FavoriteController extends Controller
{

    const PAGINATION_DEFAULT_PAGE_NUMBER = 1;
    const PAGINATION_LIMIT = 8;

    /**
     * @Route("/show/{id}", name="favorite_show")
     * @Route({"GET", "POST"})
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @param string $id
     * @param FavoriteRepository $favoriteRepository
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function show(
        Request $request,
        ApiContext $apiContext,
        string $id,
        FavoriteRepository $favoriteRepository,
        EntityManagerInterface $manager
    ): Response
    {
        $isFavorite = false;

        try {
            $data = $apiContext->getOneById($id)['data']['children'];
            if (empty($data)) {
                return $this->redirectToRoute('main_index');
            }
            $record = $data[0]['data'];
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }

        $record['favorites'] = $favoriteRepository->countByRedditId($record['name']) ?? 0;

        $form = $this->createForm(AddFavoriteType::class);

        if ($this->isGranted('ROLE_USER')) {
            /** @var User $user */
            $user = $this->getUser();

            if ($favoriteRepository->findOneByRedditIdAndUser($id, $user)) {
                $isFavorite = true;
            } else {
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $favorite = new Favorite();
                    $favorite
                        ->setRedditId($id)
                        ->setUser($user);

                    $manager->persist($favorite);
                    $manager->flush();

                    $this->addFlash(
                        'success',
                        'Реддит был успешно добавлен в "Мои избранные"'
                    );

                    return $this->redirectToRoute('favorite_show', ['id' => $id]);
                }
            }
        }

        return $this->render('favorite/show.html.twig', [
            'record' => $record,
            'isFavorite' => $isFavorite,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/show-favorites", name="favorite_show_favorites")
     * @Method("GET")
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function showFavorites(
        Request $request,
        ApiContext $apiContext,
        EntityManagerInterface $manager
    ): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $qb = $manager->createQueryBuilder();
        $qb
            ->select('f')
            ->from('App:Favorite', 'f')
            ->where('f.user = :user')
            ->setParameter('user', $user)
            ->orderBy('f.date', 'DESC');
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', self::PAGINATION_DEFAULT_PAGE_NUMBER),
            self::PAGINATION_LIMIT
        );

        $records = [];
        $ids = [];

        /** @var Favorite $favorite */
        foreach ($pagination as $favorite) {
            $ids[] = $favorite->getRedditId();
        }

        if ($ids) {
            try {
                $records = $apiContext->getAllByIds($ids)['data']['children'];
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }
        }

        return $this->render('favorite/show_favorites.html.twig', [
            'records' => $records,
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/show-all-favorites", name="favorite_show_all_favorites")
     * @Method("GET")
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @param EntityManagerInterface $manager
     * @param FavoriteRepository $favoriteRepository
     * @return Response
     */
    public function showAllFavorites(
        Request $request,
        ApiContext $apiContext,
        EntityManagerInterface $manager,
        FavoriteRepository $favoriteRepository
    ): Response
    {
        $popularRedditsId = $favoriteRepository->getPopular();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $popularRedditsId,
            $request->query->getInt('page', self::PAGINATION_DEFAULT_PAGE_NUMBER),
            self::PAGINATION_LIMIT
        );

        $ids = [];
        /** @var Favorite $favorite */
        foreach ($pagination as $favorite) {
            $ids[] = $favorite['redditId'];
        }

        try {
            $records = $apiContext->getAllByIds($ids)['data']['children'];
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }

        foreach ($records as $key => $record) {
            $records[$key]['favorites'] = $favoriteRepository->countByRedditId($record['data']['name']);
        }

        return $this->render('favorite/show_all_favorites.html.twig', [
            'records' => $records,
            'pagination' => $pagination
        ]);
    }
}

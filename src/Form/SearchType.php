<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', TextType::class, [
                'label' => false,
                'constraints' => new NotBlank(['message' => 'Укажите что искать'])
            ])
            ->add('sort', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'По релевантности' => 'relevance',
                    'Горячие' => 'hot',
                    'В топе' => 'top',
                    'Новые' => 'new',
                    'Комментирумые' => 'comments'
                ],
                'constraints' => new NotBlank(['message' => 'Укажите метод сортировки'])
            ])
            ->add('limit', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    '8шт' => 8,
                    '12шт' => 12,
                    '16шт' => 16,
                    '50шт' => 50,
                    '100шт' => 100
                ],
                'constraints' => new NotBlank(['message' => 'Укажите количество'])
            ])
            ->add('submit', SubmitType::class, ['label' => 'Искать'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

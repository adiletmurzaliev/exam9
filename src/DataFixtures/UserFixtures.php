<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    const USER_ONE = 'user1';
    const USER_TWO = 'user2';
    const USER_THREE = 'user3';
    const USER_FOUR = 'user4';
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setEmail('user1@mail.com')
            ->setPassword($this->userPasswordEncoder->encodePassword($user, '123'));

        $this->addReference(self::USER_ONE, $user);
        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user
            ->setEmail('user2@mail.com')
            ->setPassword($this->userPasswordEncoder->encodePassword($user, '123'));

        $this->addReference(self::USER_TWO, $user);
        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user
            ->setEmail('user3@mail.com')
            ->setPassword($this->userPasswordEncoder->encodePassword($user, '123'));

        $this->addReference(self::USER_THREE, $user);
        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user
            ->setEmail('user4@mail.com')
            ->setPassword($this->userPasswordEncoder->encodePassword($user, '123'));

        $this->addReference(self::USER_FOUR, $user);
        $manager->persist($user);
        $manager->flush();
    }
}

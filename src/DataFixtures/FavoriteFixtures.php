<?php

namespace App\DataFixtures;

use App\Entity\Favorite;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FavoriteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $reddits = $this->getReddits();

        /** @var User $user1 */
        $user1 = $this->getReference(UserFixtures::USER_ONE);
        /** @var User $user2 */
        $user2 = $this->getReference(UserFixtures::USER_TWO);
        /** @var User $user3 */
        $user3 = $this->getReference(UserFixtures::USER_THREE);

        foreach ($reddits as $reddit){
            $favorite = new Favorite();
            $favorite
                ->setRedditId($reddit)
                ->setUser($user1);
            $manager->persist($favorite);
            $manager->flush();
        }

        for ($i = 0; $i <= 4; $i++) {
            $favorite = new Favorite();
            $favorite
                ->setRedditId($reddits[$i])
                ->setUser($user2);
            $manager->persist($favorite);
            $manager->flush();
        }

        for ($i = 2; $i <= 6; $i++) {
            $favorite = new Favorite();
            $favorite
                ->setRedditId($reddits[$i])
                ->setUser($user3);
            $manager->persist($favorite);
            $manager->flush();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }

    private function getReddits(): array
    {
        return [
            't3_8wgone',
            't3_8w9yre',
            't3_8qc84c',
            't3_8ouvwv',
            't3_8widua',
            't3_8sh3vu',
            't3_8qaaoq',
            't3_8nr3ju',
            't3_7x1mc1',
            't3_8gncs3',
            't3_8qep03',
            't3_8uwp0l'
        ];
    }
}

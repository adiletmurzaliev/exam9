<?php

namespace App\Repository;

use App\Entity\Favorite;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favorite[]    findAll()
 * @method Favorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Favorite::class);
    }

//    /**
//     * @return Favorite[] Returns an array of Favorite objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOneByRedditIdAndUser(string $redditId, User $user): ?Favorite
    {
        try {
            return $this->createQueryBuilder('f')
                ->where('f.redditId = :redditId')
                ->setParameter('redditId', $redditId)
                ->andWhere('f.user = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function countByRedditId(string $redditId): ?int
    {
        $qb = $this->createQueryBuilder('f');

        try {
            return $qb->select($qb->expr()->count('f'))
                ->where('f.redditId = :redditId')
                ->setParameter('redditId', $redditId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getPopular(): ?array
    {
        return $this->createQueryBuilder('f')
            ->select('f.redditId, COUNT(f.id) as favorites')
            ->orderBy('favorites', 'DESC')
            ->groupBy('f.redditId')
            ->getQuery()
            ->getResult();
    }

}

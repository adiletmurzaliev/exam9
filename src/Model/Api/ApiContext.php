<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const GET_ALL_BY_FILTER = '/r/picture/search.json';
    const GET_BY_ID = '/api/info.json';

    /**
     * @param string $query
     * @param string $sort
     * @param string $limit
     * @return array
     * @throws ApiException
     */
    public function getAllByFilter(string $query, string $sort, string $limit): array
    {
        return $this->makeQuery(self::GET_ALL_BY_FILTER, self::METHOD_GET,
            [
                'q' => $query,
                'sort' => $sort,
                'limit' => $limit,
                'type' => 'link'
            ]
        );
    }

    /**
     * @param string $id
     * @return array
     * @throws ApiException
     */
    public function getOneById(string $id): array
    {
        return $this->makeQuery(self::GET_BY_ID, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @param array $ids
     * @return array
     * @throws ApiException
     */
    public function getAllByIds(array $ids): array
    {
        $query = implode(',', $ids);

        return $this->makeQuery(self::GET_BY_ID, self::METHOD_GET, ['id' => $query]);
    }
}
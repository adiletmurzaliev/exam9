<?php

use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AbstractContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function iSeeWordOnPage($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function iOnHomepage()
    {
        $this->visit($this->getContainer()->get('router')->generate('main_index'));
    }

    /**
     * @When /^я перехожу по маршруту "([^"]*)"$/
     */
    public function visitLink($arg1)
    {
        $this->visit($this->getContainer()->get('router')->generate($arg1));
    }

    /**
     * @When /^я заполняю поле формы: ([^"]*), значением: ([^"]*)$/
     * @param $field
     * @param $value
     */
    public function iFillInputFiledWithValue($field, $value)
    {
        $this->fillField($field, $value);
    }

    /**
     * @When /^я нажимаю на кнопку: ([^"]*)$/
     * @param $button
     */
    public function iPressOnButton($button)
    {
        $this->pressButton($button);
    }

    /**
     * @When /^я нажимаю на ссылку: ([^"]*)$/
     * @param $link
     */
    public function iPressOnLink($link)
    {
        try {
            $this->getSession()->getPage()->clickLink($link);
        } catch (\Behat\Mink\Exception\ElementNotFoundException $e) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я вижу элемент пагинации$/
     */
    public function iSeePagination()
    {
        if (!$this->getSession()->getPage()->find('css', '.pagination')) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я захожу на сайт как "([^"]*)" с паролем "([^"]*)"$/
     * @param $username
     * @param $password
     */
    public function iLoginAs($username, $password)
    {
        $this->visit($this->getContainer()->get('router')->generate('user_login'));
        $this->fillField('login[email]', $username);
        $this->fillField('login[plainPassword]', $password);
        $this->pressButton('Войти');
        $this->assertPageContainsText('пользователь: ' . $username);
    }

    /**
     * @When /^я вижу кнопку: ([^"]*)$/
     * @param $button
     */
    public function iSeeButton($button)
    {
        if (!$this->getSession()->getPage()->hasButton($button)) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я вижу ссылку: ([^"]*)$/
     * @param $link
     */
    public function iSeeLink($link)
    {
        if (!$this->getSession()->getPage()->hasLink($link)) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я добавляю в избранное первые записи по запросам$/
     * @param TableNode $table
     */
    public function iAddToFavorite(TableNode $table)
    {
        $hash = $table->getHash();

        foreach ($hash as $row) {
            $this->visit($this->getContainer()->get('router')->generate('main_index'));
            $this->fillField('search[query]', $row['запрос']);
            $this->pressButton('Искать');
            $this->assertPageContainsText('Автор:');
            $this->iPressOnLink('Смотреть');
            $this->assertPageContainsText('Добавить в избранное');
            $this->pressButton('Добавить в избранное');
            $this->iSeeLink('Добавлено в избранное');
        }


    }
}

